function convertirGrados() {
    var grados = parseFloat(document.getElementById("inputGrados").value);
    var conversion = document.querySelector('input[name="conversion"]:checked').value;
    var resultado = document.getElementById("resultado");
    if (conversion === "c-f") {
        var resultadoGrados = (grados * 9 / 5) + 32;
        resultado.innerHTML = "Resultado: " + resultadoGrados.toFixed(2) + " grados Fahrenheit";
    }
    else if (conversion === "f-c") {
        var resultadoGrados = (grados - 32) * 5 / 9;
        resultado.innerHTML = "Resultado: " + resultadoGrados.toFixed(2) + " grados Celsius";
    } else {
        resultado.innerHTML = "Error: Seleccione una conversión válida";
    }
}