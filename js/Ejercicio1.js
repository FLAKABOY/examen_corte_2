
//Funcion para calcular dependiendo de lo ingresado en los campos
function calcular(){
    console.log("Calculando...");
    //Obtener el numero de boletos
    var numBol = document.getElementById('numBoletos');
    var boletos = numBol.value;
    //Obtener el tipo de viaje
    var viaje = document.getElementById('tipoViaje');
    var tipoViaje = viaje.options[viaje.selectedIndex].value;
    var precio = document.getElementById('precio');
    var subTot = document.getElementById('subTot');
    var impuesto = document.getElementById('impuesto');
    var totPag = document.getElementById('totalPagar');

    switch(tipoViaje){
        case 'Sencillo':{
            console.log("Sencillo");
            precio.value = "precio: $ 950";
            subTot.value = "precio: $" + (950 * boletos);
            //Sacar el 16% de la operacion anterior
            impuesto.value = "$"+(boletos*950)*0.16;
            //poner el total a pagar
            totPag.value = "$"+((boletos*950)+((boletos*950)*0.16));
            break;
        }

        case 'Doble': {
            precio.value = "precio: $1000";
            subTot.value = "precio: $" + (1000 * boletos);
            //Sacar el 16% de la operacion anterior
            impuesto.value = "$"+(boletos*1000)*0.16;
            //poner el total a pagar
            totPag.value = "$"+((boletos*1000)+((boletos*1000)*0.16));
            break;
        }
    }
}

function limpiar(){
    var precio = document.getElementById('precio').value = "";
    var subTot = document.getElementById('subTot').value = "";
    var impuesto = document.getElementById('impuesto').value = "";
    var totPag = document.getElementById('totalPagar').value = "";
}